FROM alpine

ARG TARGETARCH

RUN apk update && apk add curl
RUN curl https://download.border0.com/linux_$TARGETARCH/border0 \
     -o /usr/local/bin/border0
RUN chmod +x /usr/local/bin/border0

CMD ["border0"]
